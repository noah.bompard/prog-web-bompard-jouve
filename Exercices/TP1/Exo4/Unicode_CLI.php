<?php

/* Script CLI détaillé */
/*
$chaine = $argv["1"] ;
echo "$chaine\n" ;
$char = mb_substr($chaine, 0, 1) ;
echo "$char\n" ;
$code_char = mb_ord($char, "utf-8") ;
echo "$code_char\n" ;
*/

/* Script CLI efficace */

echo "Le code Unicode de l'initiale du mot ", $argv["1"], " est : ", mb_ord(mb_substr($argv["1"], 0, 1)) ;

?>

