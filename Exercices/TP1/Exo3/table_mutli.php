<html>
<head>
    <tilte>Table de multiplication</tilte> <br/>
    <meta charset="UTF-8">
    <style type="text/css">
        th {
            border: 3px solid black;
            padding: 0em;
        }
        td {
            border: 0.5px solid;
            padding: 1em;
        }
        .surlignee {
            background-color: yellow;
        }

    </style>
</head>

<body>
    <form method="get">
        <label for="Row">Nombre de colonne</label> <input type="text" id="Row" name="Row"/> <br />
        <label for="Line">Nombre de ligne</label> <input type="text" id="Line" name="Line"/> <br />
        <label for="Surlignee">Mettre en evidence la ligne</label> <input type="text" id="Surlignee" name="Surlignee"/> <br/>
        <input type="submit"/>
    </form>

    <table>
        <thead>
            <tr>
                <?php

                if (isset($_GET["Row"])) {
                    $row = $_GET["Row"];
                }
                else {
                    $row = 10;
                }

                for ( $i = 0; $i<=$row; $i++) {
                    echo '<th>', $i, '</th>';
                }

                ?>
            </tr>
            
        </thead>

        <tbody>
            <?php
                if (isset($_GET["Line"])) {
                    $line = $_GET["Line"];
                }
                else {
                    $line = 10;
                }
                
                if (isset($_GET["Surlignee"])) {
                    $surlignee = $_GET["Surlignee"];
                }
                else {
                    $surlignee = 0;
                }
                
                for ( $i = 1; $i<=$line; $i++) {
                    
                    if ($i == $surlignee) {
                    echo '<tr class="surlignee">';
                    }
                    else {
                        echo '<tr>';
                    }
                    echo '<th>', $i , '</th>';
                    for ( $j = 1; $j<=$row; $j++) {
                        echo '<td>', ($j*$i), '</td>';
                    }
                    echo '</tr>';
                }
            ?>
        </tbody>
    </table>
</body>
</html>